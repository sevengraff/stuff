# Misc Programming Stuff

Hello I'm nick and this is my `stuff` folder.

## What do I use this for?

* Learning new languages
* Trying out ideas
* Understanding concepts
* Refreshing my memory on the fundamentals

It's important for a modern programmer to know a _lot_ of things. This is my playground where I try to keep up with
a fast moving industry.

## What have I learned so far?

* Groovy is pretty neat
* RabbitMQ is just as simple as the tutorial made it seem
* It is easy to forget the basics
* Emacs is for everything, everytime.